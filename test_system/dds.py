from dax.experiment import *
import artiq.coredevice.suservo
import artiq.coredevice.ad9910


class CWLaserDDSModule(DaxModule):
    """Module for a laser whose `amp` and `freq` are controlled by DDS"""
    DDS_TYPE = artiq.coredevice.ad9910.AD9910
# 
    DDS_FREQ_KEY = 'dds_freq'
    DDS_AMP_KEY = 'dds_amp'
    DDS_ATT_KEY = 'dds_att'
    DDS_LATENCY_MU_KEY = 'dds_latency_mu'
# 
    def build(self,*,channel_key:str,default_freq:float):
        self._dds: artiq.coredevice.ad9910.AD9910 = self.get_device(channel_key, self.DDS_TYPE)
        self._default_freq = default_freq
        self.update_kernel_invariants('_dds','_default_freq','_sw')

    @property
    def _sw(self):
        return self._dds.sw

    def init(self):
        # default_freq = self._dds.frequency_to_ftw(self._default_Freq)
        self._freq = self.get_dataset_sys(self.DDS_FREQ_KEY, self._default_freq)
        self._amp = self.get_dataset_sys(self.DDS_AMP_KEY, 1.0)
        self._att = self.get_dataset_sys(self.DDS_ATT_KEY, 20.)
        self.v_target = self._amp

    @kernel
    def init_kernel(self):
        self.core.reset()
        self._dds.init()
        self._dds.set_att(self._att)
        self._dds.set(self._freq,0.0,self._amp)
        self._dds.set_phase_mode(artiq.coredevice.ad9910.PHASE_MODE_CONTINUOUS)

    def post_init(self) -> None:
        pass