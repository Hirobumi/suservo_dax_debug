from dax.experiment import *
import artiq.coredevice.suservo
from test_system.dds import CWLaserDDSModule



class CWLaserSUServoModule(DaxModule):
    """Module for a laser which is controlled by an SU-Servo channel"""
    CH_TYPE = artiq.coredevice.suservo.Channel
    _TYPE = artiq.coredevice.suservo.SUServo
    SUSERVO_KP_KEY = 'suservo_kp'
    SUSERVO_KI_KEY = 'suservo_ki'
    SUSERVO_GL_KEY = 'suservo_gl'
    SUSERVO_PROFILE_KEY = 'suservo_profile'
    SUSERVO_G_KEY = 'suservo_g'
    SUSERVO_A_KEY = 'suservo_a'
    SUSERVO_KEY = 'suservo0'
    SUSERVO_V_TARGET_KEY = 'suservo_v_t'
    SUSERVO_FREQ_KEY = 'suservo_freq'
    SUSERVO_PROFILE_KEY = 'suservo_profile'

    def build(self,*,channel_key:str,adc_channel:int):
        self._adc_channel = adc_channel
        self._suservo_ch = self.get_device(channel_key, self.CH_TYPE)
        self._suservo = self.get_device(self.SUSERVO_KEY, self._TYPE)
        self.update_kernel_invariants('_adc_channel', '_suservo','_suservo_ch')

    def init(self):
        self._kp: float = self.get_dataset_sys(self.SUSERVO_KP_KEY, -0.07)
        self._ki: float = self.get_dataset_sys(self.SUSERVO_KI_KEY, -6000.)
        self._gl: float = self.get_dataset_sys(self.SUSERVO_GL_KEY, 0.)
        self._g: int = self.get_dataset_sys(self.SUSERVO_G_KEY, 0)
        
        self._a: float = self.get_dataset_sys(self.SUSERVO_A_KEY, 0.)
        _v_t: float = self.get_dataset_sys(self.SUSERVO_V_TARGET_KEY, 2.05)
        self._freq: float = self.get_dataset_sys(self.SUSERVO_FREQ_KEY, 110*MHz)
        self._profile: int = self.get_dataset_sys(self.SUSERVO_PROFILE_KEY, 0)
        self._offset = -_v_t*(10**(self._g-1))
        self.o_target = self._offset
        self.update_kernel_invariants('_profile')
        # Set physical parameters
        

    @kernel
    def init_kernel(self):
        self.core.reset()
        self._suservo.init()
        self._suservo.set_config(enable=1)
        self._suservo.set_pgia_mu(1, self._g)
        self._suservo.cplds[0].set_att(1, self._a)
        self._suservo_ch.set_iir(profile=self._profile, adc=self._adc_channel,kp=self._kp,ki=self._ki,g=self._gl)
        self.p(self._profile)
        self._suservo_ch.set_dds(profile=self._profile,frequency=self._freq,offset=self._offset)
        delay(10*ms)
        self._suservo_ch.set(en_out=1,en_iir=1,profile=self._profile)
        # self._suservo.cplds[0].io_update.pulse(10*us)

    def post_init(self) -> None:
        pass

    """Module functionality"""
    @kernel
    def release_and_hold(self, t:TInt64):
        y = self._suservo_ch.get_y_mu(self._profile)
        delay(10*us)
        self._suservo_ch.set(en_out=1,en_iir=0,profile=self._profile)
        delay(10*us)
        self._suservo_ch.set_y_mu(0,y)
        delay(10*us)
        self._suservo_ch.set(en_out=0)
        delay_mu(t)
        self._suservo_ch.set(en_out=1)
        delay(50*us)
        self._suservo_ch.set(en_out=1,en_iir=1,profile=self._profile)

    @kernel
    def off(self):
        self._suservo_ch.set(en_out=0,en_iir=0,profile=self._profile)

    @kernel
    def on(self):
        # self.p(self._suservo.get_adc(self._adc_channel))
        self.p(self._suservo_ch.get_y(self._profile))
        delay(1*ms)
        self._suservo_ch.set(en_out=1,en_iir=1,profile=self._profile)
        # self.p(self._suservo.get_adc(self._adc_channel))

    @rpc(flags={"async"})
    def p(self,a):
        print(a)

    @kernel
    def on_without_iir(self):
        self._suservo_ch.set_y_mu(self._profile,50000)
        self._suservo_ch.set(en_out=1,en_iir=0,profile=self._profile)
        # self.p(self._suservo_ch.get_y(profile=self._profile))
        self.p(self._suservo.get_adc(self._adc_channel))

    @kernel
    def set_offset(self,offset):
        self._suservo_ch.set_dds(profile=self._profile,frequency=self._freq,offset=offset)

    @kernel
    def ramp_amp(self,o_target, t, t_step=100*us):
        o_start = self.o_target
        self.o_target = o_target
        n = int(t//(t_step))
        step = (o_target-o_start)/n
        # self.p([v_target,v_start])
        for i in range(n):
            self.set_offset(o_start+i*step)
            delay(t_step)

    @kernel
    def set_kp_ki(self,kp,ki):
        self._suservo_ch.set_iir(profile=self._profile, adc=self._adc_channel,kp=kp,ki=ki,g=self._gl)

    @kernel
    def get_adc(self):
        return self._suservo.get_adc(self._adc_channel)


class TestSystem(DaxSystem):
    SYS_ID='test'
    SYS_VER=1
    DAX_INFLUX_DB_KEY = None

    def build(self):
        super(TestSystem,self).build()
        self.test_tweezer = CWLaserSUServoModule(self, 'tweezer', channel_key="suservo0_ch1",adc_channel=1)
        self.laser = CWLaserDDSModule(self, 'laser', channel_key="urukul4_ch3",default_freq=110*MHz)
        self.update_kernel_invariants('laser')
        # print(self.test_tweezer._suservo.cplds[0].io_update)

    @kernel
    def init(self):
        self.test_tweezer.init_kernel()
        self.laser.init_kernel()