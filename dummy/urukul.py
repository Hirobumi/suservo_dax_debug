from artiq.coredevice import urukul, ad9910, spi2


class CPLD(urukul.CPLD):
    def dummy(self):
        pass


class AD9910(ad9910.AD9910):
    pass

class SPIMaster(spi2.SPIMaster):
    pass
