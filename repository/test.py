import sys
import os
sys.path.append("/home/isidor/artiq/suservo_debug")
from test_system.system import *
from test_system.system import ms,us
sys.path.remove("/home/isidor/artiq/suservo_debug")
import numpy as np


class TweezerStabilizationDebug(TestSystem, Experiment):
    def build(self):
        super(TweezerStabilizationDebug,self).build()
        self.kp = self.get_argument("Kp", NumberValue(-0.07))
        self.ki = self.get_argument("Ki", NumberValue(-6000.0))

    @rpc(flags={"async"})
    def save(self, d):
        with open("data.txt", "a+") as f:
            f.write(f"{d}\n")

    @rpc(flags={"async"})
    def delete(self):
        if "data.txt" in os.listdir():
            os.remove("data.txt")

    @rpc(flags={"async"})
    def std(self, kp, ki):
        data = np.loadtxt("data.txt")
        # plt.plot(data)
        print(f"## {kp}, {ki}")
        print(data[:5])
        print(f"{data.std():.3}", data.mean())
    
    def run(self):
        self.dax_init()
        self._run()

    @kernel
    def _run(self):
        self.delete()
        self.core.reset()
        self.core.break_realtime()
        # self.test_tweezer.set_kp_ki(self.kp, self.ki)
        delay(10*ms)
        self.test_tweezer.on()
        delay(10*ms)
        for i in range(10):
            self.save(self.test_tweezer.get_adc())
            # self.suservo0.get_adc(0)
            delay(1000*ms)
        delay(200*us)
        self.std(self.kp, self.ki)

