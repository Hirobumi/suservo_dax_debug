from artiq.experiment import *
import numpy as np
import matplotlib.pyplot as plt
import os


class TweezerStabilization(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.setattr_device("urukul2_cpld")
        self.setattr_device("suservo0")
        self.setattr_device("suservo0_ch1")

    @rpc(flags={"async"})
    def save(self, d):
        with open("data.txt", "a+") as f:
            f.write(f"{d}\n")

    @rpc(flags={"async"})
    def delete(self):
        if "data.txt" in os.listdir():
            os.remove("data.txt")

    @rpc(flags={"async"})
    def std(self, kp, ki):
        data = np.loadtxt("data.txt")
        # plt.plot(data)
        print(f"## {kp}, {ki}")
        print(f"{data.std():.3}", data.mean())

    def run(self):
        pl = np.linspace(-5000, -40000,10)[:1]
        for p in pl:
            self._run(-2.5, p)
    
    @kernel
    def _run(self, kp, ki):
        # Prepare core
        self.delete()
        self.core.reset()

        #Initialize and activate SUServo
        self.suservo0.init()
        self.suservo0.set_config(enable=1)
        # print(self.suservo0_ch0.get_y(0))
        # Set Sampler gain and Urukul attenuation
        g = 0
        A = 10.0
        self.suservo0.set_pgia_mu(1, g)         # set gain on Sampler channel 0 to 10^g
        # self.suservo0.set_pgia_mu(1, g) 
        self.suservo0.cplds[0].set_att(1, A)       # set attenuation on Urukul channel 0 to 0
        # self.suservo0.cpld0.set_att(1, A)
        
        
        # Set physical parameters
        v_t = 2.3                     # target input voltage (V) for Sampler channel
        f = 110000000.0                         # frequency (Hz) of Urukul output
        o = -v_t*(10.0**(g-1))                  # offset to assign to servo to reach target voltage
        # o = -v_t

        # Set PI loop parameters 
        kp = -.07                            # proportional gain in loop
        ki = -6000.0/s                              # integrator gain
        gl = 0.                                # integrator gain limit
        adc_ch = 1                              # Sampler channel to read from
        
        # Input parameters, activate Urukul output (en_out=1),
        # activate PI loop (en_iir=1)
        self.suservo0_ch1.set_iir(profile=0, adc=adc_ch, kp=kp, ki=ki, g=gl)
        self.suservo0_ch1.set_dds(profile=0, frequency=f, offset=o)
        # self.suservo0_ch0.set_dds(profile=1, frequency=f, offset=o)
        # self.suservo0_ch0.set_dds_offset_mu(0,-5000)
        # self.suservo0_ch1.set(en_out=1, en_iir=0, profile=0)
        # self.suservo0_ch0.set_y(0,0.5)
        
        delay(10*ms)
        delay(1*ms) 
        # self.suservo0_ch0.set(en_out=1, en_iir=0, profile=1)
        self.suservo0_ch1.set(en_out=1, en_iir=1, profile=0)
        # self.suservo0_ch1.set_y(0,0.1)
        # self.suservo0.set_config(enable=1)
        delay(10*ms)
        y = self.suservo0_ch1.get_y(0)
        delay(10*ms)
        for i in range(5000):
            self.save(self.suservo0.get_adc(adc_ch))
            # self.suservo0.get_adc(0)
            delay(1*ms)
        delay(200*us)
        self.std(kp, ki)
        print(y)
       

    def analysis(self):
        data = np.loadtxt("data.txt")
        plt.plot(data[:100])
        plt.show()