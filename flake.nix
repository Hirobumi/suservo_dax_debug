{
  inputs.dax.url = "git+https://gitlab.com/duke-artiq/dax.git";
  outputs = { self, dax}:
    let
      pkgs = dax.inputs.artiqpkgs.inputs.nixpkgs.legacyPackages.x86_64-linux;
      aqmain = dax.inputs.artiqpkgs.packages.x86_64-linux;
      daxmain = dax.packages.x86_64-linux;
    in {
      defaultPackage.x86_64-linux = pkgs.buildEnv {
        name = "artiq-env";
        paths = [
          # ========================================
          # EDIT BELOW
          # ========================================
          (pkgs.python3.withPackages(ps: [
            # List desired Python packages here.
            aqmain.artiq
            ps.paramiko  # needed if and only if flashing boards remotely (artiq_flash -H)
            daxmain.flake8-artiq
            daxmain.artiq-stubs
            daxmain.dax
            # The NixOS package collection contains many other packages that you may find
            # interesting. Here are some examples:
            ps.pandas
            ps.numpy
            ps.scipy
            ps.notebook
            ps.jupyter
            #ps.numba
            # ps.matplotlib
            # or if you need Qt (will recompile):
            # (ps.matplotlib.override { enableQt = true; })
            ps.bokeh
            #ps.cirq
            #ps.qiskit
          ]))
          #aqextra.korad_ka3005p
          #aqextra.novatech409b
          # List desired non-Python packages here
          aqmain.openocd-bscanspi  # needed if and only if flashing boards
          # Other potentially interesting packages from the NixOS package collection:
          pkgs.gtkwave
          #pkgs.spyder
          #pkgs.R
          #pkgs.julia
          # ========================================
          # EDIT ABOVE
          # ========================================
        ];
      };
    };
}
